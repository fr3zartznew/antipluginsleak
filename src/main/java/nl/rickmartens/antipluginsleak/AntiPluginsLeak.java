package nl.rickmartens.antipluginsleak;

import nl.rickmartens.antipluginsleak.listeners.ChatListener;
import nl.rickmartens.antipluginsleak.utils.NMSUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public final class AntiPluginsLeak extends JavaPlugin {

    public static AntiPluginsLeak pl;
    public static void unloadEvents() { HandlerList.unregisterAll((Plugin) AntiPluginsLeak.pl); }

    /*
     * Supported versions(NMS)
     * https://www.spigotmc.org/wiki/spigot-nms-and-minecraft-versions/
     */
    private static final String[] SUPPORTED_VERSIONS = {
            "v1_8_R1", // 1.8

            "v1_8_R2", // 1.8.3

            "v1_8_R3", // 1.8.4 and higher

            "v1_9_R1", // 1.9

            "v1_9_R2", // 1.9.4

            "v1_10_R1", // 1.10.x

            "v1_11_R1", // 1.11.x

            "v1_12_R1", // 1.12.x
    };

    @Override
    public void onEnable() {
        PluginManager plman = Bukkit.getPluginManager();
        pl = this;

        getLogger().info("NMS Version is " + NMSUtil.NMS_VERSION + ", ignore this message.");
        if (!Arrays.asList(SUPPORTED_VERSIONS).contains(NMSUtil.NMS_VERSION)) {
            getLogger().severe("NMS Version not supported, Shutting DOWN!");
            plman.disablePlugin(AntiPluginsLeak.pl);
            unloadEvents();
        }

        plman.registerEvents(new ChatListener(), this);

        getServer().getConsoleSender().sendMessage("[AntiPluginsLeak] " + ChatColor.DARK_GREEN + "Enabled " + ChatColor.RESET + "AntiPluginsLeak v" + getDescription().getVersion());
    }

    @Override
    public void onDisable() {
        getServer().getConsoleSender().sendMessage("[AntiPluginsLeak] " + ChatColor.DARK_RED + "Disabled " + ChatColor.RESET + "AntiPluginsLeak v" + getDescription().getVersion());
    }
}
