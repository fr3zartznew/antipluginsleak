package nl.rickmartens.antipluginsleak.utils;

import org.bukkit.Bukkit;

public class NMSUtil {

    public static final String NMS_VERSION;

    static {
        NMS_VERSION = Bukkit.getServer().getClass().getPackage().getName().substring(23);
    }

}
